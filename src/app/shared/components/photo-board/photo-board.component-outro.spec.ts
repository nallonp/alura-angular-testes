import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, ViewChild } from '@angular/core';

import { PhotoBoardComponent } from './photo-board.component';
import { PhotoBoardModule } from './photo-board.module';
import { Photo } from './interfaces/photo';
import { buildPhotoList } from './test/build-photo-list';

describe(PhotoBoardComponent.name, () => {
  let component: PhotoBoardTestComponent;
  let fixture: ComponentFixture<PhotoBoardTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PhotoBoardTestComponent],
      imports: [PhotoBoardModule]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoBoardTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it(`Should display rows and columns when (@Input photos) has value.`, () => {
    component.photos = buildPhotoList();
    fixture.detectChanges();
    expect(component.board?.rows.length).withContext('Number of rows').toBe(2);
    expect(component.board?.rows[0].length)
      .withContext('Number of columns of the first row')
      .toBe(4);
    expect(component.board?.rows[1].length)
      .withContext('Number of columns of the second row')
      .toBe(4);
  });
});

@Component({
  template: ` <app-photo-board [photos]="photos"> </app-photo-board> `
})
class PhotoBoardTestComponent {
  @ViewChild(PhotoBoardComponent) public board?: PhotoBoardComponent;
  public photos: Photo[] = [];
}
