import { UniqueIdService } from './unique-id.service';

describe(UniqueIdService.name, () => {
  let service: UniqueIdService;

  beforeEach(() => {
    service = new UniqueIdService();
  });

  it(`#${UniqueIdService.prototype.generateUniqueIdWithPrefix.name} 
  should generate an id when called with prefix.`, () => {
    const id = service.generateUniqueIdWithPrefix('app');
    const guidRegex = new RegExp(
      '^app-[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$'
    );
    expect(guidRegex.test(id)).toBeTrue();
  });

  it(`#${UniqueIdService.prototype.generateUniqueIdWithPrefix.name} 
  should not generate duplicate IDs when called multiple times.`, () => {
    const ids = new Set();
    for (let i = 0; i < 50; i++) {
      ids.add(service.generateUniqueIdWithPrefix('app-'));
    }
    expect(ids.size).toBe(50);
  });

  it(`#${UniqueIdService.prototype.generateUniqueIdWithPrefix.name} 
  should throw when called with empty.`, () => {
    const emptyValues = ['', '0', '1'];
    emptyValues.forEach((emptyValue) =>
      expect(() => service.generateUniqueIdWithPrefix(emptyValue))
        .withContext(`Value "${emptyValue}"`)
        .toThrow()
    );
    // expect(() => service.generateUniqueIdWithPrefix('')).toThrow();
  });

  it(`#${UniqueIdService.prototype.getNumberOfGeneratedUniqueIds.name} 
  should return the number of generated IDs when called.`, () => {
    service.generateUniqueIdWithPrefix('app-');
    service.generateUniqueIdWithPrefix('app-');
    expect(service.getNumberOfGeneratedUniqueIds()).toBe(2);
  });
});
