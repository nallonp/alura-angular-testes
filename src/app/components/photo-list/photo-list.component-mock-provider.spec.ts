import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { PhotoListComponent } from './photo-list.component';
import { PhotoListModule } from './photo-list.module';
import { PhotoBoardService } from '../../shared/components/photo-board/services/photo-board.service';
import { Observable, of } from 'rxjs';
import { Photo } from '../../shared/components/photo-board/interfaces/photo';
import { buildPhotoList } from '../../shared/components/photo-board/test/build-photo-list';
import { PhotoBoardMockService } from '../../shared/components/photo-board/services/photo-board-mock.service';

describe(PhotoListComponent.name + ' Mock Provider', () => {
  let component: PhotoListComponent;
  let fixture: ComponentFixture<PhotoListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PhotoListModule, HttpClientModule],
      providers: [
        {
          provide: PhotoBoardService,
          useClass: PhotoBoardMockService
        }
        /*{
          provide: PhotoBoardService,
          useValue: {
            getPhotos(): Observable<Photo[]> {
              return of(buildPhotoList());
            }
          }
        }*/
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoListComponent);
    component = fixture.componentInstance;
  });

  it('Should create component', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it(`(D) Should display board when data arrives.`, () => {
    fixture.detectChanges(); // deve ser chamado depois que o serviço for modificado
    const board = fixture.nativeElement.querySelector('app-photo-board');
    const loader = fixture.nativeElement.querySelector('.loader');
    expect(board).withContext('Should display board.').not.toBeNull();
    expect(loader).withContext('Should not display loader.').toBeNull();
  });
});
